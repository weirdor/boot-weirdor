package com.boot.weirdor.elastic.repository;


import com.boot.weirdor.elastic.index.EsComment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by weirdor  on 17/8/7.
 */
@Repository
public interface EsCommentRepository extends ElasticsearchRepository<EsComment, Long> {
}
