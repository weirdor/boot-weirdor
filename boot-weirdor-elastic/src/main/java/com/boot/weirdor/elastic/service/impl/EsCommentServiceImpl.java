package com.boot.weirdor.elastic.service.impl;



import com.boot.weirdor.elastic.index.EsComment;
import com.boot.weirdor.elastic.repository.EsCommentRepository;
import com.boot.weirdor.elastic.service.EsCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by weirdor on 17/8/7.
 */
@Service
public class EsCommentServiceImpl implements EsCommentService {

    @Autowired
    private EsCommentRepository esCommentRepository;


    public void index(List<EsComment> list) {
        esCommentRepository.save(list);
    }
}
