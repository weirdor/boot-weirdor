package com.boot.weirdor.elastic.service;




import com.boot.weirdor.elastic.index.EsComment;

import java.util.List;

/**
 * Created by weirdor  on 17/8/7.
 */
public interface EsCommentService {

    /**
     * 创建索引
     * @param list
     */
    public void index(List<EsComment> list);
}
