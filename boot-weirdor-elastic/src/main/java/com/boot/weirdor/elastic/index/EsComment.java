package com.boot.weirdor.elastic.index;


import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;


/**
 * Created by weirdor  on 17/8/7.
 */

@Document(indexName = "idx_s", type = "es_comment", shards = 3, replicas = 1)
public class EsComment {

    @Id
    @ApiModelProperty("主键编号")
    private Long id;

    @ApiModelProperty("唯一编号")
    private String uuid;


    public void setId(Long id) {
        this.id = id;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public Long getId() {
        return id;
    }
}
