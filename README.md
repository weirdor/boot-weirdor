#weirdor基础权限开发平台
	Generate自动生成Entity、Mapper、Service、Controller让开发更简单
	
## 模块介绍
* weirdor-common  通用工具类、以及一些轻量级组件
* weirdor-www     提供restapi和 前端视图页面
* weirdor-module  基于DB
* weirdor-facade  数据逻辑整合, 依赖底层Service操作
* weirdor-elastic 基于elastic存储数据


## 内置功能

1. 系统基础管理
   - 1.1 用户管理 
   - 1.2 角色管理 
   - 1.3 资源管理
   - 1.4 部门管理 
   - 1.5 定时任务 
   
2. 系统监控管理
   - 2.1 druid监控 
   
3. 日志管理
   - 3.1 用户登录日志 
   
## 技术选型

1、后端

* 核心框架：Spring Boot
* 安全框架：Apache Shiro
* 持久层框架： Mybatis plus
* 模板引擎：freemarker
* 数据库连接池：Alibaba Druid
* 缓存框架：Redis
* 日志管理：Log4j2
* 任务调度: Quartz
* 工具类：  hutool
* 验证: hibernate validator


2、前端
* 前端框架: layer ui

# 历史记录

## 2017年08月2日
* 起步ing......
## 2017年08月8日
* 新增elasticsearch模块