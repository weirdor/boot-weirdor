package com.boot.weirdor.common.utill;

/**
 * Created by weirdor on 2017/8/18.
 */
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import com.qiniu.common.Zone;
import com.qiniu.storage.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Recorder;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.persistent.FileRecorder;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;

/**
 * 七牛文件上传
 *
 * @author weirdor
 * 2017年06月29日14:52:20
 *  开发文档：https://developer.qiniu.com/
 */
public class qiNiuutil {

    private static Logger logger = LoggerFactory.getLogger(qiNiuutil.class);
    // 设置好账号的ACCESS_KEY和SECRET_KEY
    private final static String ACCESS_KEY = "J_KpvvtDLwP_8TLLVG90q5AHEVbO-ZjLpJrgA9zK";
    private final static String SECRET_KEY = "X4gXFQ3z-KfHOOEtcVI7E0MEEchijJgwFl9kRN-q";

    // 域名
    private final static String domainOfBucket = "http://osaefn3rh.bkt.clouddn.com/";
    // 密匙配置
    private final static Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
   //构造一个带指定Zone对象的配置类（zone2为华南）
   private final static   Configuration cfg = new Configuration(Zone.zone2());
    // 创建上传对象
    private final static UploadManager uploadManager = new UploadManager(cfg);


    /**
     * @author weirdor 覆盖上传凭证
     * @param bucketname
     *            空间名称
     * @param key
     *            上传到七牛后保存的文件名
     * 2017年06月29日14:52:28
     */
    public String getUpToken(String bucketname, String key) {

        return auth.uploadToken(bucketname, key);

    }

    /**
     * @author weirdor 简单上传凭证
     * @param bucketname
     *            空间名称
     * 2017年06月29日14:52:14
     */
    public static String getUpToken(String bucketname) {
        StringMap putPolicy = new StringMap();
        // 自定义上传回复凭证
        putPolicy.put("returnBody",
                "{\"key\":\"$(key)\",\"hash\":\"$(etag)\",\"bucket\":\"$(bucket)\",\"fsize\":$(fsize)}");
        long expireSeconds = 3600;
        return auth.uploadToken(bucketname, null, expireSeconds, putPolicy);

    }

    /**
     * @author weirdor 简单上传
     * @param filePath
     *            文件路径 （也可以是字节数组、或者File对象）
     * @param key
     *            上传到七牛上的文件的名称 （同一个空间下，名称【key】是唯一的）
     * @param bucketName
     *            空间名称 （这里是为了获取上传凭证）
     *
     * 2017年06月29日14:52:08
     */
    public static Object simpleupload(String filePath, String key, String bucketName) {
        Map<String, Object> map=new HashMap<String, Object>();
        try {
            String token = getUpToken(bucketName);
            // 调用put方法上传
            Response res = uploadManager.put(filePath, key, token);
            if (res.statusCode == 200 && res.isOK()) {
                logger.debug("上传成功");
                map.put("url", domainOfBucket+key);
                map.put("result", "success");
            }
        } catch (QiniuException e) {

            e.printStackTrace();
            map.put("result", "fail");
        }
        return map;
    }

    /**
     * @author weirdor 断点续传
     * @throws IOException
     * @param filePath
     *            文件路径 （也可以是字节数组、或者File对象）
     * @param key
     *            上传到七牛上的文件的名称 （同一个空间下，名称【key】是唯一的）
     * @param bucketName
     *            空间名称 （这里是为了获取上传凭证）
     * @2017年06月29日14:51:10
     *
     */

    public static Object breakpointUpload(String filePath, String key, String bucketName) throws IOException {
        Map<String, Object> map=new HashMap<String, Object>();
        // 设置凭证
        String bucket = "weirdor";
        // 设置一个临时文件存放地址
        String localTempDir = Paths.get(System.getenv("java.io.tmpdir"), bucket).toString();
        // 实例化recorder对象
        Recorder recorder = new FileRecorder(localTempDir);
        // 实例化上传对象，并且传入一个recorder对象
        UploadManager uploadManager = new UploadManager(cfg,recorder);
        // token值
        String token = getUpToken(bucketName);
        try {
            // 调用put方法上传
            Response res = uploadManager.put(filePath, key, token);
            // 是否上传成功
            if (res.statusCode == 200 && res.isOK()) {
                logger.debug("上传成功");
                map.put("url", domainOfBucket+key);
                map.put("result", "success");
            }
        } catch (QiniuException e) {
            logger.debug("上传失败");
            e.printStackTrace();
            map.put("result", "fail");
        }
        return map;

    }

    //测试
    public static void main(String[] args) throws IOException {

        //简单上传
        //Object map=simpleupload("/Users/weirdor/jmongo/src/main/java/com/jmongo/config/MainConfig.java","3.java","asstes");
        //System.out.println(map);
        // 断点续传
        Object map= breakpointUpload("/Users/weirdor/Downloads/googlechrome_mac_55.0.2883.95.dmg",
                "Postman.dmg",
                "asstes");
        System.out.println(map);
    }
}