package com.boot.weirdor.common.erro;

/**
 * 常用的Http状态码作为code
 * 
 * @author weirdor
 *
 */
public enum DefaultHttpErrorCode implements BootWeirdorErrorCode {

	CLIENT_ARGUMENT_ERROR(400, "请求参数错误"), CLIENT_ERROR_401(401, "请求未授权"), CLIENT_ERROR_403(403,
			"请求被禁止"), 
	NOT_EXISTS(404, "请求资源不存在"), 
	CONFLICT(409, "请求资源已存在"), 
	CLIENT_ERROR_415(415, "请求资源类型错误"),

	SERVER_ERROR(500, "服务端错误");

	// =================================================

	private int code;
	private String message;

	private DefaultHttpErrorCode(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "错误码：" + code + ", 错误信息：" + message;
	}
}
