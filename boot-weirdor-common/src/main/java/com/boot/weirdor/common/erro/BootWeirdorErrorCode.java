package com.boot.weirdor.common.erro;

/**
 * Created by weirdor on 2017/8/20.
 * <p>
 * 错误码接口，应包含该一个code和一条描述信息.该接口的实现主要和BootWeirdorException及其子类结合使用，充当错误信息描述符的角色。
 * </p>
 * <p>
 * 具体实现类可选复写toString方法，该方法的返回值会影响BootWeirdorException的getMessage方法的返回值。
 * </p>
 * <p>
 * 应结合具体业务场景定义ErrorCode
 */


public interface BootWeirdorErrorCode {


    /**
     * 返回具体的错误代码
     *
     * @return
     */
    int getCode();

    /**
     * 错误代码的描述信息
     *
     * @return
     */
    String getMessage();
}
