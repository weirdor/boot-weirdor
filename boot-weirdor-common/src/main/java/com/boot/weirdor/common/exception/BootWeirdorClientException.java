package com.boot.weirdor.common.exception;


import com.boot.weirdor.common.erro.BootWeirdorErrorCode;

/**
 * 客户端异常基类
 * 
 * @author weirdor
 *
 */
public class BootWeirdorClientException extends BootWeirdorException {

	private static final long serialVersionUID = 1L;

	/**
	 * 用带 {@link BootWeirdorErrorCode} 的构造器替代
	 * 
	 * @param message
	 */
	@Deprecated
	public BootWeirdorClientException(String message) {
		super(message);
	}

	/**
	 * 用带 {@link BootWeirdorErrorCode} 的构造器替代
	 * 
	 * @param
	 */
	@Deprecated
	public BootWeirdorClientException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * 用带 {@link BootWeirdorErrorCode} 的构造器替代
	 * 
	 * @param message
	 */
	@Deprecated
	public BootWeirdorClientException(String message, Throwable throwable) {
		super(message, throwable);
	}

	/**
	 * 用带 {@link BootWeirdorErrorCode} 的构造器替代
	 * 
	 * @param message
	 */
	@Deprecated
	public BootWeirdorClientException(String message, Throwable throwable, int code) {
		super(message, throwable);
	}

	// ---------------------------------------------------------------------------------

	public BootWeirdorClientException(BootWeirdorErrorCode errorCode) {
		// 未对参数验证，虽可能存在空指针异常，即使存在也是我们内部问题，下同
		this(errorCode, errorCode.toString());
	}

	public BootWeirdorClientException(BootWeirdorErrorCode errorCode, String excMsg) {
		super(errorCode, excMsg);
	}

	public BootWeirdorClientException(Throwable throwable, BootWeirdorErrorCode errorCode) {
		this(throwable, errorCode, errorCode.toString());
	}

	public BootWeirdorClientException(Throwable throwable, BootWeirdorErrorCode errorCode, String excMsg) {
		super(throwable, errorCode, excMsg);
	}

}
