package com.boot.weirdor.common.exception;

import com.boot.weirdor.common.erro.BootWeirdorErrorCode;
import com.boot.weirdor.common.erro.DefaultHttpErrorCode;

/**
 * 最会游Java项目自定义异常基类
 * 
 * @author weidor
 *
 */
public abstract class BootWeirdorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	// 设置默认值，默认是服务端错误，否则调用getErrorCode()可能会导致空指针异常
	protected BootWeirdorErrorCode errorCode = DefaultHttpErrorCode.SERVER_ERROR;

	/**
	 * 用带 {@link BootWeirdorErrorCode} 的构造器替代
	 * 
	 * @param message
	 */
	@Deprecated
	public BootWeirdorException(String message) {
		super(message);
	}

	/**
	 * 用带 {@link BootWeirdorErrorCode} 的构造器替代
	 * 
	 * @param throwable
	 */
	@Deprecated
	public BootWeirdorException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * 用带 {@link BootWeirdorErrorCode} 的构造器替代
	 * 
	 * @param message
	 * @param throwable
	 */
	@Deprecated
	public BootWeirdorException(String message, Throwable throwable) {
		super(message, throwable);
	}

	// -------------------------------------------------------------------------------------------------

	/**
	 * 构建ZuihuiyouException，将用errorCode的描述信息作为异常的描述信息
	 * 
	 * @param errorCode
	 *            错误码
	 */
	public BootWeirdorException(BootWeirdorErrorCode errorCode) {
		// 未对参数验证，虽可能存在空指针异常，即使存在也是我们内部问题，下同
		this(errorCode, errorCode.toString());
	}

	public BootWeirdorException(BootWeirdorErrorCode errorCode, String excMsg) {
		super(excMsg);
		this.errorCode = errorCode;
	}

	public BootWeirdorException(Throwable throwable, BootWeirdorErrorCode errorCode) {
		this(throwable, errorCode, errorCode.toString());
	}

	public BootWeirdorException(Throwable throwable, BootWeirdorErrorCode errorCode, String excMsg) {
		super(excMsg, throwable);
		this.errorCode = errorCode;
	}

	public int getErrorCode() {
		return errorCode.getCode();
	}
}
