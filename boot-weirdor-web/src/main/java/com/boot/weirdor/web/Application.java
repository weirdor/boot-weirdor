package com.boot.weirdor.web;

import com.boot.weirdor.web.context.listener.ApplicationReadyEventListener;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by weirdor on 17/8/1.
 */
@EnableAsync
@Configuration
@ComponentScan
@EnableScheduling
@SpringBootApplication
@EnableAspectJAutoProxy  //表示开启AOP代理自动配置
@EnableAutoConfiguration
@ImportResource(value = "classpath:spring/spring-bean.xml")
@EnableElasticsearchRepositories(basePackages = "com.boot.weirdor.elastic.repository")

public class Application extends SpringBootServletInitializer{

    @Autowired
    private ElasticsearchOperations es;

    protected final static Logger LOG = LoggerFactory.getLogger(Application.class);


    public static void main( String[] args )
    {
        System.out.println("***************************************************************");
        System.out.println("启动boot-weirdor...");

        SpringApplication app = new SpringApplication(Application.class);

        app.addListeners(new ApplicationReadyEventListener());

        app.run(args);
    }

}