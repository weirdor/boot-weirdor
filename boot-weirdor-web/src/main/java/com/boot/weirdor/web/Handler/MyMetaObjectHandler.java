package com.boot.weirdor.web.Handler;

/**
 * Created by weirdor on 2017/8/20.
 */

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import com.boot.weirdor.module.entity.User;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MyMetaObjectHandler extends MetaObjectHandler {
    //新增填充

    @Override
    public void insertFill(MetaObject metaObject) {

        Object lastUpdateTime = metaObject.getValue("createTime");
        //获取当前登录用户

        User user = (User) SecurityUtils.getSubject().getPrincipal();
        System.out.print(user);
        if (null == lastUpdateTime) {
            metaObject.setValue("createTime", new Date());
        }
    }

    //更新填充

    @Override
    public void updateFill(MetaObject metaObject) {
        insertFill(metaObject);
    }
}