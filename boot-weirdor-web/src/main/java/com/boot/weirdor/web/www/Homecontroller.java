package com.boot.weirdor.web.www;

import com.boot.weirdor.elastic.index.EsComment;
import com.boot.weirdor.elastic.service.EsCommentService;
import com.boot.weirdor.web.base.BaseController;
import com.boot.weirdor.web.util.CaptchaUtil;
import com.feilong.core.Validator;
import com.xiaoleilu.hutool.http.HttpResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by weirdor on 2017/8/1.
 */
@Controller
public class Homecontroller extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(Homecontroller.class);

    @Autowired
    private EsCommentService ed;

    @RequestMapping(value = "/boot-weirdor.html", method = RequestMethod.GET)
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("index");
        return modelAndView;
    }

    //elastic 的式例
    @RequestMapping(value = "/p", method = RequestMethod.GET)
    public ModelAndView ho(ModelAndView modelAndView) {
        modelAndView.setViewName("index");
        List<EsComment> esCommentList = new ArrayList<EsComment>();
        EsComment e = new EsComment();
        e.setId(1L);
        e.setUuid("ssss");
        esCommentList.add(e);
        ed.index(esCommentList);
        modelAndView.addObject(esCommentList);
        return modelAndView;
    }


    @RequestMapping(value = { "/usersPage", "" })
    public String usersPage() {
        return "user/allUsers";
    }

    @RequestMapping("/rolesPage")
    public String rolesPage() {
        return "role/roleinfo";
    }

    @RequestMapping("/resourcesPage")
    public String resourcesPage() {
        return "resources/resourcesinfo";
    }

    @RequestMapping("/403")
    public String forbidden() {
        return "403";
    }


}
