package com.boot.weirdor.web.context.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

/**
 * 应用就绪事件监听器，
 * 
 * @author weirdor
 *
 */
public class ApplicationReadyEventListener implements ApplicationListener<ApplicationReadyEvent> {

	private Logger logger = LoggerFactory.getLogger(ApplicationReadyEventListener.class);

	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		logger.info("+++++++++++++++++++++++  boot-weirdor启动完成   +++++++++++++++++++++++");
		logger.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");


	}

}
