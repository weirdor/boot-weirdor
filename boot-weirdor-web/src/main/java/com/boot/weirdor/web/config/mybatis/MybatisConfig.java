package com.boot.weirdor.web.config.mybatis;


import com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.spring.MybatisMapperRefresh;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;

import javax.sql.DataSource;

/**
 * mybatispuls配置
 * 
 * @author weirdor
 * @date 2017年08月02日17:10:10
 */
@Configuration
@MapperScan("com.boot.weirdor.module.mapper*")
public class MybatisConfig implements EnvironmentAware {

	@Autowired(required = false)
	private  DataSource dataSource;

	@Autowired
	private Environment environment;

	private RelaxedPropertyResolver propertyResolver;

    public void setEnvironment(Environment environment) {
        this.environment = environment;
        this.propertyResolver = new RelaxedPropertyResolver(environment,"spring.datasource.");//配置前缀
	  }

	/**

	 *	 mybatis-plus分页插件

	 */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		PaginationInterceptor page = new PaginationInterceptor();
		page.setDialectType("mysql");
		return page;
	}

}