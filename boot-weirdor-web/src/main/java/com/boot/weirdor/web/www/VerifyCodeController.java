package com.boot.weirdor.web.www;

import com.boot.weirdor.web.base.BaseController;
import com.boot.weirdor.web.util.CaptchaUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by weirdor on 2017/8/3.
 */
@RestController
public class VerifyCodeController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(Homecontroller.class);

    @ResponseBody
    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    public void verify(HttpServletRequest request, HttpServletResponse response) {
        try {
            String verifyCode = CaptchaUtil.outputImage(response.getOutputStream());
            request.getSession().setAttribute("verify", verifyCode);//把验证码存入session
            System.out.println("验证码:"+verifyCode);
            logger.debug(String.format("verify code: %s", verifyCode));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}