package com.boot.weirdor.facade.shiro.bean;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * Created by weirdor on 17/8/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShiroUser {

    //用户主键账号
    private long id;

    //登录账号")
    private String username;

    //登录后展示名称
    private String showName;

    //真实姓名
    private String realName;

    //头像
    private  String healUrl;

    //拥有的资源列表
    private Set<String> urls;

    //拥有的角色列表
    private Set<String> roles;



    public ShiroUser(String showName) {
        this.showName = showName;
    }

    /**
     * 本函数输出将作为默认的<shiro:principal/>输出.
     */
    @Override
    public String toString() {
        return this.showName;
    }
}
