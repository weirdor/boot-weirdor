package com.boot.weirdor.module.mapper;

import com.boot.weirdor.module.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 角色 Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface RoleMapper extends BaseMapper<Role> {

    List<Map<Long, String>> selectResourceListByRoleId(@Param("id") Long id);

    List<Long> selectResourceIdListByRoleId(@Param("id") Long id);

}