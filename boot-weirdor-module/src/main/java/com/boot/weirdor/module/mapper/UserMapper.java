package com.boot.weirdor.module.mapper;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.boot.weirdor.module.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import javafx.scene.control.Pagination;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 用户 Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface UserMapper extends BaseMapper<User> {

    List<Map<String, Object>> selectUserPage(Page<User> page, Map<String, Object> params);


}