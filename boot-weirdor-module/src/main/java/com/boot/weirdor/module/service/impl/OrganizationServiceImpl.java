package com.boot.weirdor.module.service.impl;

import com.boot.weirdor.module.entity.Organization;
import com.boot.weirdor.module.mapper.OrganizationMapper;
import com.boot.weirdor.module.service.IOrganizationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 组织机构 服务实现类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
@Service
public class OrganizationServiceImpl extends ServiceImpl<OrganizationMapper, Organization> implements IOrganizationService {
	
}
