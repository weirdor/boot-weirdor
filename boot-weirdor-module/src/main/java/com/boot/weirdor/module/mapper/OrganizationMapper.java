package com.boot.weirdor.module.mapper;

import com.boot.weirdor.module.entity.Organization;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 组织机构 Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface OrganizationMapper extends BaseMapper<Organization> {

}