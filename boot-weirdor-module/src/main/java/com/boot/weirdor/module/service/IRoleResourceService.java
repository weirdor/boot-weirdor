package com.boot.weirdor.module.service;

import com.boot.weirdor.module.entity.RoleResource;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 角色资源 服务类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface IRoleResourceService extends IService<RoleResource> {
	
}
