package com.boot.weirdor.module.mapper;

import com.boot.weirdor.module.entity.UserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
  * 用户角色 Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    List<Long> selectRoleIdListByUserId(@Param("userId") Long userId);

}