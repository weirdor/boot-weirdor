package com.boot.weirdor.module.service.impl;

import com.boot.weirdor.module.entity.SysLog;
import com.boot.weirdor.module.mapper.SysLogMapper;
import com.boot.weirdor.module.service.ISysLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {
	
}
