package com.boot.weirdor.module.mapper;

import com.boot.weirdor.module.entity.Resource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 资源 Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface ResourceMapper extends BaseMapper<Resource> {

}