package com.boot.weirdor.module.service;

import com.boot.weirdor.module.entity.User;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface IUserService extends IService<User> {
    /**
     * 根据用户账号获取用户信息
     * @param login_name
     * @return
     */
    public User selectByUserName(String login_name);


}
