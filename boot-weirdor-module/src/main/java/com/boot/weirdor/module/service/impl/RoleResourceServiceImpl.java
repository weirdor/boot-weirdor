package com.boot.weirdor.module.service.impl;

import com.boot.weirdor.module.entity.RoleResource;
import com.boot.weirdor.module.mapper.RoleResourceMapper;
import com.boot.weirdor.module.service.IRoleResourceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色资源 服务实现类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
@Service
public class RoleResourceServiceImpl extends ServiceImpl<RoleResourceMapper, RoleResource> implements IRoleResourceService {
	
}
