package com.boot.weirdor.module.service.impl;

import com.boot.weirdor.module.entity.UserRole;
import com.boot.weirdor.module.mapper.UserRoleMapper;
import com.boot.weirdor.module.service.IUserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色 服务实现类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {
	
}
