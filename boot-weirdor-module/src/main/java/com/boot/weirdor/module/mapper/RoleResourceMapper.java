package com.boot.weirdor.module.mapper;

import com.boot.weirdor.module.entity.RoleResource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 角色资源 Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface RoleResourceMapper extends BaseMapper<RoleResource> {

}