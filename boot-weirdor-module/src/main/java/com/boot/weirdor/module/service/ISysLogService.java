package com.boot.weirdor.module.service;

import com.boot.weirdor.module.entity.SysLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface ISysLogService extends IService<SysLog> {
	
}
