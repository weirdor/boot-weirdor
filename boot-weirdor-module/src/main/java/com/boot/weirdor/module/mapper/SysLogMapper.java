package com.boot.weirdor.module.mapper;

import com.boot.weirdor.module.entity.SysLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 系统日志 Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}