package com.boot.weirdor.module.service.impl;

import com.boot.weirdor.module.entity.Resource;
import com.boot.weirdor.module.mapper.ResourceMapper;
import com.boot.weirdor.module.service.IResourceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源 服务实现类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService {
	
}
