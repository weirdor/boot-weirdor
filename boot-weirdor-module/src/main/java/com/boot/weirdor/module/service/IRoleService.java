package com.boot.weirdor.module.service;

import com.boot.weirdor.module.entity.Role;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface IRoleService extends IService<Role> {

    Map<String,Set<String>> selectResourceMapByUserId(Long id);

    List<Role> selectAll();

    List<Long> selectResourceIdListByRoleId(Long id);
}
