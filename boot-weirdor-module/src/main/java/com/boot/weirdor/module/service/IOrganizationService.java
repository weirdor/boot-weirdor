package com.boot.weirdor.module.service;

import com.boot.weirdor.module.entity.Organization;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 组织机构 服务类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface IOrganizationService extends IService<Organization> {
	
}
