package com.boot.weirdor.module.service;

import com.boot.weirdor.module.entity.Resource;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 资源 服务类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface IResourceService extends IService<Resource> {
	
}
