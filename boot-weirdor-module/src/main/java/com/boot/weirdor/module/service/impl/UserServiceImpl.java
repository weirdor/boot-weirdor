package com.boot.weirdor.module.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.boot.weirdor.common.utill.DigestUtils;
import com.boot.weirdor.common.utill.PasswordUtil;
import com.boot.weirdor.module.entity.User;
import com.boot.weirdor.module.mapper.UserMapper;
import com.boot.weirdor.module.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Autowired
    private IUserService userService;

    public User selectByUserName(String login_name) {
        if (StringUtils.isBlank(login_name)) {
            return null;
        }
        EntityWrapper<User> wrapper = new EntityWrapper<User>();
        wrapper.eq("login_name", login_name);
        return userService.selectOne(wrapper);

    }


    public boolean insert(User user) {
        //设置salt
        String salt = PasswordUtil.Salt();
        user.setSalt(salt);
        String password = new Md5Hash(user.getPassword(), salt).toString();
        //设置密码
        user.setPassword(password);
        //填填充创建时间字段
        user.setCreateTime(new Date());
        //设置为暂无头像
        if(null==user.getHeadUrl()){
            user.setHeadUrl("http://ov0jx0037.bkt.clouddn.com/timg.jpeg");
        }
        return super.insert(user);
    }

    public boolean update(User user, Wrapper<User> wrapper) {
        //设置salt
        String salt = PasswordUtil.Salt();
        user.setSalt(salt);
        String password = new Md5Hash(user.getPassword(), salt).toString();
        //设置密码
        user.setPassword(password);
        return super.update(user, wrapper);
    }
}
