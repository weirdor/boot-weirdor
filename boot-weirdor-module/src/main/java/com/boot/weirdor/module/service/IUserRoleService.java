package com.boot.weirdor.module.service;

import com.boot.weirdor.module.entity.UserRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author weirdor
 * @since 2017-08-09
 */
public interface IUserRoleService extends IService<UserRole> {
	
}
